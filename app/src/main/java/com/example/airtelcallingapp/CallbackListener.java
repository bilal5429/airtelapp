package com.example.airtelcallingapp;

public interface CallbackListener {
    public void updateView(boolean success);
}