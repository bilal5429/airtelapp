package com.example.airtelcallingapp;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    static MediaPlayer mediaPlayer;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //  String imei = android.os.SystemProperties.get(android.telephony.TelephonyProperties.PROPERTY_IMSI);
        try {
            System.out.println("kakakaka = " + getSim1IMSI());
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        mediaPlayer = MediaPlayer.create(this, R.raw.ringtone);
        mediaPlayer.start();

        View[] images = {findViewById(R.id.img4), findViewById(R.id.img3), findViewById(R.id.img2), findViewById(R.id.img1),}; //array of views that we want to animate

        //we will have 2 animator foreach view, fade in & fade out
        //prepare animators - creating array of animators & instantiating Object animators
        ArrayList<ObjectAnimator> anims = new ArrayList<>(images.length * 2);
        for (View v : images)
            anims.add(ObjectAnimator.ofFloat(v, View.ALPHA, 0f, 1f).setDuration(80)); //fade in animator
        for (View v : images)
            anims.add(ObjectAnimator.ofFloat(v, View.ALPHA, 1f, 0f).setDuration(80)); //fade out animator

        final AnimatorSet set = new AnimatorSet(); //create Animator set object
        //if we want to repeat the animations then we set listener to start again in 'onAnimationEnd' method
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                set.start(); //repeat animator set indefinitely
            }
        });

        set.setStartDelay(600); //set delay every time we start the chain of animations

        for (int i = 0; i < anims.size() - 1; i++)
            set.play(anims.get(i)).before(anims.get(i + 1)); //put all animations in set by order (from first to last)
        set.start();


        //////////////////////////////////////////////////////////////////////////

        //      final ImageButton callReceiveButton = findViewById(R.id.answer_call_button);
        final ImageButton callHangUpButton = findViewById(R.id.hangup_call_button);
        final TextView incomingCall = findViewById(R.id.callInfo);


//        callReceiveButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent i = new Intent();
//                i.setClass(MainActivity.this, MainActivity2.class);
//                startActivity(i);
//                mediaPlayer.stop();
//                finish();
//            }
//        });

        callHangUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaPlayer.stop();
                finish();
            }
        });

        ImageTouchSlider slider = (ImageTouchSlider) findViewById(R.id.slider1);
        slider.setOnImageSliderChangedListener(new ImageTouchSlider.OnImageSliderChangedListener() {

            @Override
            public void onChanged() {
                // do something what you want here.
            }

        });
    }

    public void xyz() {
    }


    public static String get(Context context, String key) {
        String ret = "";

        try {
            ClassLoader cl = context.getClassLoader();
            @SuppressWarnings("rawtypes")
            Class SystemProperties = cl.loadClass("android.os.SystemProperties");

            //Parameters Types
            @SuppressWarnings("rawtypes")
            Class[] paramTypes = new Class[1];
            paramTypes[0] = String.class;

            Method get = SystemProperties.getMethod("get", paramTypes);

            //Parameters
            Object[] params = new Object[1];
            params[0] = new String(key);

            ret = (String) get.invoke(SystemProperties, params);
        } catch (Exception e) {
            ret = "";
            //TODO : Error handling
        }

        return ret;
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
    public String getSim1IMSI() throws InvocationTargetException, IllegalAccessException {
        String imsi = null;
        if (checkAndRequestPermissions()) {
            TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            Method getSubId = null;
            SubscriptionManager sm = null;
            try {
                getSubId = TelephonyManager.class.getMethod("getSubscriberId", int.class);
                sm = (SubscriptionManager) getSystemService(TELEPHONY_SUBSCRIPTION_SERVICE);


            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return "cc";
            }

            imsi = (String) getSubId.invoke(tm, sm.getActiveSubscriptionInfoForSimSlotIndex(0).getSubscriptionId()); // Sim slot 1 IMSI
            return imsi;
        }

        return imsi;
    }

    private  boolean checkAndRequestPermissions() {
        List listPermissionsNeeded = new ArrayList<>();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this,
                        (String[]) listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                        1);
                return false;
            }
            else {return  true;}
        }
        else {
            return true;
        }
    }


}
