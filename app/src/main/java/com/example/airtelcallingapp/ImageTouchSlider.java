package com.example.airtelcallingapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ImageTouchSlider extends LinearLayout implements View.OnTouchListener {

    private Context mContext;

    private ImageView mImage, img1,img2,img3,img4;
    private int mScreenWidthInPixel;
    private int mScreenHeightInPixel;
    private int mScreenWidthInDp;
    private int mScreenHeightInDp;
    private float mDensity;

    private int mPaddingInDp = 15;
    private int mPaddingInPixel;

    private int mLengthOfSlider;
    int originalMargin = 0;
    float yPreviousPosition = 0;
    float xPreviousPosition = 0;


    public interface OnImageSliderChangedListener{
        void onChanged();
    }

    private OnImageSliderChangedListener mOnImageSliderChangedListener;

    public ImageTouchSlider(Context context) {
        super(context);
        mContext = context;
        createView();
    }

    public ImageTouchSlider(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        createView();
    }

    public ImageTouchSlider(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        createView();
    }

    public void createView() {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
       inflater.inflate(R.layout.image_touch_slider, this, true);

        mImage = (ImageView) findViewById(R.id.slider);
        mImage.setOnTouchListener(this);
        img1 = (ImageView) findViewById(R.id.img1);
        img2 = (ImageView) findViewById(R.id.img2);
        img3 = (ImageView) findViewById(R.id.img3);
        img4 = (ImageView) findViewById(R.id.img4);


        WindowManager manager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics ();
        display.getMetrics(outMetrics);

        mDensity  = getResources().getDisplayMetrics().density;
        float dpWidth  = outMetrics.widthPixels / mDensity;

        mScreenWidthInPixel = outMetrics.widthPixels;
        mScreenHeightInPixel = outMetrics.heightPixels;

        mScreenHeightInDp = (int) (mScreenHeightInPixel / mDensity);

        mLengthOfSlider = 0;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        LayoutParams layoutParams = (LayoutParams) v.getLayoutParams();
        int width = v.getWidth();
        int height = v.getHeight();
        float xPos = event.getRawX();
        float yPos = event.getRawY();
//        yPreviousPosition = mImage.getY();
        System.out.println("hello cheetah mScreenHeightInDp = "+mScreenHeightInDp);
        System.out.println("hello cheetah mDensity="+mDensity);
        System.out.println("hello cheetah height="+height);
        System.out.println("hello cheetah xPos="+xPos);
        System.out.println("hello cheetah yPos="+yPos);
        System.out.println("hello cheetah////////////////////////////////");
        System.out.println("hello cheetah////////////////////////////////");

        switch(event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                 originalMargin = layoutParams.topMargin;
                yPreviousPosition = yPos;
                xPreviousPosition = xPos;
                img1.setVisibility(INVISIBLE);
                img2.setVisibility(INVISIBLE);
                img3.setVisibility(INVISIBLE);
                img4.setVisibility(INVISIBLE);
                System.out.println("hello cheetah yPreviousPosition="+yPos);
                System.out.println("hello cheetah xPreviousPosition="+yPos);

                // You can add some clicked reaction here.
                break;
            case MotionEvent.ACTION_MOVE:
                mLengthOfSlider =mLengthOfSlider +10;
                yPos = event.getRawY();

                System.out.println("hello cheetah inside yPos="+yPos+"   height="+height);

//                if(yPos < (mScreenHeightInPixel - height - mPaddingInDp*mDensity) && yPos > mPaddingInDp*mDensity) {

                if(yPos < yPreviousPosition && xPos> xPreviousPosition-20 && xPos< xPreviousPosition+20 ){
                    mOnImageSliderChangedListener.onChanged();
System.out.println("cat sliderrrLength="+mLengthOfSlider +"   "+mScreenHeightInDp/2);
                    if(mLengthOfSlider< mScreenHeightInDp/2) {
                        layoutParams.topMargin = layoutParams.topMargin - 20;
                        mImage.setLayoutParams(layoutParams);
                    }
                }
//                else if(yPos > (mScreenHeightInPixel - height - mPaddingInDp*mDensity) && yPos < mPaddingInDp*mDensity) {
//                    layoutParams.topMargin = layoutParams.topMargin - 20;
//                    mImage.setLayoutParams(layoutParams);
//                }
                break;
            case MotionEvent.ACTION_UP:
                mLengthOfSlider = 0;
                layoutParams.topMargin = originalMargin;
                mImage.setLayoutParams(layoutParams);
                img1.setVisibility(VISIBLE);
                img2.setVisibility(VISIBLE);
                img3.setVisibility(VISIBLE);
                img4.setVisibility(VISIBLE);

                Intent intent = new Intent();
                intent.setClass(mContext,MainActivity2.class);
                mContext.startActivity(intent);
                ((Activity)mContext).finish();
                MainActivity.mediaPlayer.stop();
                break;
            default:
                break;
        }

        return true;
    }

    public void setOnImageSliderChangedListener(OnImageSliderChangedListener listener) {
        mOnImageSliderChangedListener = listener;
    }

} //end of class